#!/bin/bash
#welcome to zackn9nes jamf hostname script
#if using with JSS make a policy
#put hashed username:pass in $4 base64
#put jssurl in $5, include the https:// or else
#if jamf is not on the target host it will run in native mode so ignore the above 2 things
#if not using with jamf set MANUAL_LOCATION prompts you on readline
#if not using with jamf set MANUAL_ASSET_TAG prompts you on readline

#JAMF_MODE logic
JAMF_BINARY=$(/usr/bin/which jamf)
if test -f "$JAMF_BINARY"; then
    echo "$JAMF_BINARY exists"
    JAMF_MODE=true
else
	echo "No JAMF found on system!"
	JAMF_MODE=false

    read -p "Enter your location manually default [NY]: " MANUAL_LOCATION
    LOCATION=${MANUAL_LOCATION:-NY}
    echo $LOCATION

    read -p "Enter your asset tag manually default [1]: " MANUAL_ASSET_TAG
    ASSET_TAG=${MANUAL_ASSET_TAG:-1}
    echo $ASSET_TAG
fi

# jss variables set?
if [ "$JAMF_MODE" == true ]; then
	# sanity check
	if [ -z "$4" ]
	then
		echo "You forgot to fill user/pass out in jamf"
	    exit 1
	elif [ -z "$5" ]
	then
	     echo "You forgot to put jamfpro URL in jamf"
	     exit 1
	fi
fi

# jamf API subset/location and subset/general
if [ "$JAMF_MODE" == true ]; then
    jssCredsHash=$4 # hash your JamfPro username:password with base64
    jssHost=$5 #put jssurl here, include the https:// or else

    # Get the endpoints serial 
    fullSerialForAPI=$(ioreg -rd1 -c IOPlatformExpertDevice | awk -F'"' '/IOPlatformSerialNumber/{print $4}')

    # Query API for serial's city and asset tag properties
	TYPE_XML="Accept: text/xml"
	BASIC_AUTH="Authorization: Basic ${jssCredsHash}"
	BASE_REQUEST="${jssHost}/JSSResource/computers/serialnumber/${fullSerialForAPI}/subset"

    LOCATION_REQ=$(/usr/bin/curl --silent -H "$TYPE_XML" -H "$BASIC_AUTH" "$BASE_REQUEST/location")
	LOCATION=$(echo $LOCATION_REQ | xmllint --format - 2>/dev/null | awk -F'>|<' '/<building>/{print $3}'|cut -f1 -d"@")
    echo "Detected JSS location > building is: $LOCATION"

    REAL_NAME=$(echo $LOCATION_REQ | xmllint --format - 2>/dev/null | awk -F'>|<' '/<real_name>/{print $3}'|cut -f1 -d"@")
    echo "Detected JSS location > real name is: $REAL_NAME"

    ASSET_TAG=$(/usr/bin/curl --silent -H "$TYPE_XML" -H "$BASIC_AUTH" "$BASE_REQUEST/general" | xmllint --format - 2>/dev/null | awk -F'>|<' '/<asset_tag>/{print $3}'|cut -f1 -d"@")
    echo "Detected JSS general > asset tag is: $ASSET_TAG"
elif [ "$JAMF_MODE" == false ]; then
    if [ -z "$LOCATION" ]; then
	    echo "You forgot to set a manual location!"
	else
	    echo "Setting location manually to: $LOCATION"
    fi
    if [ -z "$ASSET_TAG" ]; then
	    echo "You forgot to set a manual asset tag!"
	else
	    echo "Setting location manually to $ASSET_TAG"
    fi
fi

# get machines info locally
INITIAL_HW_EVAL=$(sysctl hw.model)
if [[ $INITIAL_HW_EVAL == *"Pro"* ]]; then
    echo "Model: MBP"
    MODEL="MBP"
elif [[ $INITIAL_HW_EVAL == *"Air"* ]]; then
    echo "Model: MBA"
    MODEL="MBA"
elif [[ $INITIAL_HW_EVAL == *"MacBook8"* ]]; then
    echo "Model: MB12"
    MODEL="MB"
elif [[ $INITIAL_HW_EVAL == *"MacBook9"* ]]; then
    echo "Model: MB12"
    MODEL="MB"
elif [[ $INITIAL_HW_EVAL == *"MacBook10"* ]]; then
    echo "Model: MB12"
    MODEL="MB"
elif [[ $INITIAL_HW_EVAL == *"iMac"* ]]; then
    echo "Model: iMac"
    MODEL="iMac"
elif [[ $INITIAL_HW_EVAL == *"mini"* ]]; then
    echo "Model: mini"
    MODEL="Mini"
elif [ -z "$INITIAL_HW_EVAL" ]; then
    echo "computer unknown"
	exit 1
fi

# get last four serial for year
APPLE_SERIAL_ENDING=$(system_profiler SPHardwareDataType | awk '/Serial/ {print $4}' | grep -o '....$')
echo "Last four is: " $APPLE_SERIAL_ENDING

# curl apples machine db against last 4 of serial
MNF_YEAR=$(curl --silent "https://support-sp.apple.com/sp/product?cc=`echo $APPLE_SERIAL_ENDING`" |grep -Eo '[0-9]{4}')
echo "Determined year is: " $MNF_YEAR

# check user locally
USER=$(scutil <<< "show State:/Users/ConsoleUser" | awk -F': ' '/[[:space:]]+Name[[:space:]]:/ { if ( $2 != "loginwindow" ) { print $2 }}')
echo "Detected user is: " $USER

# sanitize user
USER=$(echo $USER | sed 's/[^a-zA-Z0-9]//g')

# killswitch Error:
if [ -z "$USER" ]
then
	echo "Error: user is null exiting with error"
    exit 1
elif [ $USER = "root" ]
then
     echo "Error: user is root user failing gracefully"
     exit 1
elif [ $USER = "splash" ]
then
     echo "Error: user is splash user failing gracefully"
     exit 1
elif ((MNF_YEAR <= 2000 && MNF_YEAR >= 2030)); then
     echo "Expected year is out of range, just giving up on the year"
     $MNF_YEAR = ''
elif [ -z "$LOCATION" ] && [ "$JAMF_MODE" == true ];
then
	echo "Error: Location is broken for JAMF_MODE exiting with error $LOCATION"
    exit 1
elif [ -z "$MODEL" ]
then
	echo "Error: Model is broken exiting with error"
    exit 1
fi

#######################################################################################
# Rename Mac
#######################################################################################
COMPANY="Barrel"
CITY="$LOCATION"
NAMES=($REAL_NAME)
FIRST_NAME=${NAMES[0]}

HOST_NAME="$COMPANY-$MODEL-$ASSET_TAG-$FIRST_NAME";
echo "Host Name will be: ${HOST_NAME}"

COMPUTER_NAME="$COMPANY $MODEL $ASSET_TAG ($FIRST_NAME)";
echo "Computer Name will be: ${COMPUTER_NAME}"

/usr/sbin/scutil --set ComputerName "$COMPUTER_NAME"
/usr/sbin/scutil --set LocalHostName "$HOST_NAME"
/usr/sbin/scutil --set HostName "$HOST_NAME"
dscacheutil -flushcache

if [ "$JAMF_MODE" == true ]; then
#	$JAMF_BINARY setComputerName -name "$HOST_NAME"
	$JAMF_BINARY recon
elif [ "$JAMF_MODE" == false ]; then
	while true; do
	    read -p "Do you wish to delete this program off the disk?" yN
	    case $yN in
		[Yy]* ) rm hostname.sh; break;;
		[Nn]* ) exit;;
		* ) echo "Please answer yes or no.";;
	    esac
	done
fi

exit 0